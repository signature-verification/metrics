MCYT_DATA = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/data/MCYT/"
MOBISIG_DATA = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/data/MOBISIG/"

PROCESSED_DATA = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/data/PROCESSED/"

MCYT_GENUINE = PROCESSED_DATA + "mcyt_genuine.csv"
MCYT_FORGERY = PROCESSED_DATA + "mcyt_forgery.csv"

MOBISIG_GENUINE = PROCESSED_DATA + "mobisig_genuine.csv"
MOBISIG_FORGERY = PROCESSED_DATA + "mobisig_forgery.csv"

MODEL_OUTPUT_DIRECTORY = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/metrics/model/"
MODEL_PATH = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/metrics/model/best_model_mobisig.hdf5"
MODEL_OUTPUT_PATH = "/home/mozesbotond/WorkSpace/SignatureVerification/Repos/metrics/model/model.tflite"

MCYT_FIELDS = [0, 1, 2]
MOBISIG_FIELDS = [0, 1, 2, 3]

LENGTH = 512
DIMENSIONS = 2
RANDOM_STATE = 42
