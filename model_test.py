import os
import model
import processing
import pandas as pd
import numpy as np
import random

import matplotlib.pyplot as plt

import settings
import utils


def get_manhattan_distance(s1, s2):
    distance = 0.0
    for i in range(len(s1)):
        distance += abs(s1[i] - s2[i])
    return distance/len(s1)


def get_manhattan_distances(template, test):
    distances = []
    for s2 in test:
        distance = 0.0
        for s1 in template:
            distance += get_manhattan_distance(s1, s2)
        distances.append(distance/len(template))

    return distances


def get_scores(distances):
    scores = []
    for d in distances:
        scores.append(1/(1+d))
    return scores


def get_thresholds(min, unit, length):
    thresholds = [min]

    for i in range(length-1):
        thresholds.append(thresholds[i]+unit)

    return thresholds


def get_false_positive(thresholds, scores_forgery):
    FP = []
    for t in thresholds:
        FP.append(np.count_nonzero(scores_forgery >= t))
    return FP


def get_false_negative(thresholds, scores_genuine):
    FN = []
    for t in thresholds:
        FN.append(np.count_nonzero(scores_genuine < t))
    return FN


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        return -1, -1

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div
    return x, y


def extract_features(extractor, df):
    data = np.asarray(df).astype(np.float32)
    data = data.reshape(-1, settings.LENGTH, settings.DIMENSIONS)
    return extractor.predict(data)


def test_template_model(signature_number, df_genuine, df_forgery, mode):
    length = 20
    users = df_genuine['user'].unique()

    extractor = model.get_extract_model("best_model_mobisig.hdf5")
    EER = []

    for user in users:
        # print("USER: " + str(user))
        signature_list_genuine = df_genuine.loc[df_genuine['user'] == user].reset_index(
        )
        signature_list_forgery = df_forgery.loc[df_forgery['user'] == user].reset_index(
        )

        if mode == "ML":
            template = extract_features(extractor, signature_list_genuine.loc[0:
                                                                              signature_number-1, 0:1023])
            test_genuine = extract_features(extractor,
                                            signature_list_genuine.loc[signature_number:, 0:1023])
            test_forgery = extract_features(extractor,
                                            signature_list_forgery.loc[0:19, 0:1023])
        else:
            template = signature_list_genuine.loc[0:
                                                  signature_number-1, 0:1023].values

            test_genuine = signature_list_genuine.loc[signature_number:, 0:1023].values
            test_forgery = signature_list_forgery.loc[0:19, 0:1023].values

        distances_genuine = get_manhattan_distances(template, test_genuine)
        distances_forgery = get_manhattan_distances(template, test_forgery)

        scores_genuine = get_scores(distances_genuine)
        scores_forgery = get_scores(distances_forgery)

        min = np.min([np.min(scores_genuine), np.min(scores_forgery)])
        max = np.max([np.max(scores_genuine), np.max(scores_forgery)])

        # print("Minimum = ", str(min))
        # print("Maximum = ", str(max))
        # print("Max-Min = " + str(max-min))
        unit = (max-min)/length

        # print("Unit = " + str(unit))

        thresholds = get_thresholds(min, unit, length)

        FP = get_false_positive(thresholds, scores_forgery)
        FN = get_false_negative(thresholds, scores_genuine)

        # print("False Positive: " + str(FP))
        # print("False Negative: " + str(FN))

        FPR = [x/len(scores_forgery) for x in FP]
        FNR = [x/len(scores_genuine) for x in FN]

        # print("False Positive Rate: " + str(FPR))
        # print("False Negative Rate: " + str(FNR))

        x, y = 0, 0

        for i in range(length):
            if FPR[i] < FNR[i]:
                # print(str(FPR[i-1]) + " - " + str(FNR[i-1]))
                # print(str(FPR[i]) + " - " + str(FNR[i]))
                line1 = [[thresholds[i-1], FPR[i-1]], [thresholds[i], FPR[i]]]
                line2 = [[thresholds[i-1], FNR[i-1]], [thresholds[i], FNR[i]]]
                x, y = line_intersection(line1, line2)
                # print(str(x) + " - " + str(y))
                break

        # plt.plot(thresholds, FPR)
        # # plt.plot(thresholds, FPR, '*')
        # plt.plot(thresholds, FNR)
        # # plt.plot(thresholds, FNR, '*')
        # plt.plot(x, y, 'o')
        # plt.legend(['FAR', 'FRR'], loc='upper right')
        # plt.savefig('images/EER.png', )
        # plt.show()

        EER.append(y)

    return np.average(EER), np.std(EER)


def get_random_signatures(df_genuine, user):
    df = pd.DataFrame()
    users = df_genuine['user'].unique()
    counter = 0
    for i in range(20):
        if user == users[counter]:
            counter += 1

        df_temp = df_genuine.loc[df_genuine['user']
                                 == users[counter]].reset_index()
        df = df.append(df_temp.loc[0, 0:1023])
        counter += 1
    return df


def test_template_model_random(signature_number, df_genuine, mode):
    length = 10
    users = df_genuine['user'].unique()

    extractor = model.get_extract_model("best_model_mcyt.hdf5")
    EER = []

    for user in users:

        # print("USER: " + str(user))
        signature_list_genuine = df_genuine.loc[df_genuine['user'] == user].reset_index(
        )
        signature_list_forgery = get_random_signatures(df_genuine, user)
        if mode == "ML":
            template = extract_features(extractor, signature_list_genuine.loc[0:
                                                                              signature_number-1, 0:1023])
            test_genuine = extract_features(extractor,
                                            signature_list_genuine.loc[signature_number:, 0:1023])
            test_forgery = extract_features(extractor, signature_list_forgery)
        else:
            template = signature_list_genuine.loc[0:
                                                  signature_number-1, 0:1023].values

            test_genuine = signature_list_genuine.loc[signature_number:, 0:1023].values
            test_forgery = signature_list_forgery.values

        distances_genuine = get_manhattan_distances(template, test_genuine)
        distances_forgery = get_manhattan_distances(template, test_forgery)

        scores_genuine = get_scores(distances_genuine)
        scores_forgery = get_scores(distances_forgery)

        # print("Scores genuine: " + str(scores_genuine))
        # print("Scores forgery: " + str(scores_forgery))

        min = np.min([np.min(scores_genuine), np.min(scores_forgery)])
        max = np.max([np.max(scores_genuine), np.max(scores_forgery)])

        # print("Minimum = ", str(min))
        # print("Maximum = ", str(max))
        # print("Max-Min = " + str(max-min))
        unit = (max-min)/length

        # print("Unit = " + str(unit))

        thresholds = get_thresholds(min, unit, length)

        FP = get_false_positive(thresholds, scores_forgery)
        FN = get_false_negative(thresholds, scores_genuine)

        # print("False Positive: " + str(FP))
        # print("False Negative: " + str(FN))

        FPR = [x/len(scores_forgery) for x in FP]
        FNR = [x/len(scores_genuine) for x in FN]

        # print("False Positive Rate: " + str(FPR))
        # print("False Negative Rate: " + str(FNR))

        x, y = 0, 0

        for i in range(length):
            if FPR[i] < FNR[i]:
                # print(str(FPR[i-1]) + " - " + str(FNR[i-1]))
                # print(str(FPR[i]) + " - " + str(FNR[i]))
                line1 = [[thresholds[i-1], FPR[i-1]], [thresholds[i], FPR[i]]]
                line2 = [[thresholds[i-1], FNR[i-1]], [thresholds[i], FNR[i]]]
                x, y = line_intersection(line1, line2)
                # print(str(x) + " - " + str(y))
                break

        # plt.plot(thresholds, FPR)
        # plt.plot(thresholds, FPR, '*')
        # plt.plot(thresholds, FNR)
        # plt.plot(thresholds, FNR, '*')
        # plt.plot(x, y, 'o')
        # plt.legend(['FAR', 'FRR'], loc='upper right')
        # plt.savefig('images/EER.png', )
        # plt.show()

        EER.append(y)

    return np.average(EER), np.std(EER)


def test_template_models(df_genuine, df_forgery):
    result = []
    for num in [5, 10, 15]:
        avg, std = test_template_model(num, df_genuine, df_forgery, "ML")
        result.append([num, avg, std, "ML", "MCYT"])
        avg, std = test_template_model(num, df_genuine, df_forgery, "TM")
        result.append([num, avg, std, "TM", "MCYT"])

    df = pd.DataFrame(
        result, columns=["Template number", "Average", "Std", "Mode", "Dataset"])
    df.to_csv("./EER_MCYT.csv")


def test_template_models_random(df_genuine):
    result = []
    for num in [5, 10, 15]:
        avg, std = test_template_model_random(num, df_genuine, "ML")
        result.append([num, avg, std, "ML", "MCYT"])
        avg, std = test_template_model_random(num, df_genuine, "TM")
        result.append([num, avg, std, "TM", "MCYT"])

    df = pd.DataFrame(
        result, columns=["Template number", "Average", "Std", "Mode", "Dataset"])
    df.to_csv("./EER_random_MOBISIG.csv")


df_genuine = pd.read_csv(settings.MOBISIG_GENUINE, header=None)
df_forgery = pd.read_csv(settings.MOBISIG_FORGERY, header=None)
df_genuine = utils.normalize_rows_signature(df_genuine, "ZSCORE")
df_forgery = utils.normalize_rows_signature(df_forgery, "ZSCORE")

# test_template_models(df_genuine, df_forgery)
test_template_models_random(df_genuine)
