import processing
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import settings
import utils
import os


def signature_lengths():
    # MCYT lengths
    folder_list = utils.listdirs(settings.MCYT_DATA)
    lengths = []
    for folder in folder_list:
        signature_list = os.listdir(settings.MCYT_DATA + folder)

        if len(signature_list) != 0:
            # print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MCYT_DATA +
                                 folder + "/" + file1, usecols=settings.MCYT_FIELDS)

                if(file1[4] == "v"):
                    lengths.append(len(df))

    counts, bins = np.histogram(lengths, 100)

    plt.hist(bins[:-1], 100, weights=counts)
    plt.xlabel("Aláírások hossza")
    plt.ylabel("Aláírások száma")
    plt.savefig('images/mcyt_length.png')
    plt.clf()

    avg = np.average(lengths)
    print("MCYT average length: " + str(avg))

    std = 0
    for l in lengths:
        std += (l-avg)**2

    std /= len(lengths)

    std = std**0.5

    print("MCYT standard deviation: " + str(std))

    # MOBISIG lengths
    folder_list = utils.listdirs(settings.MOBISIG_DATA)
    lengths = []
    for folder in folder_list:
        signature_list = os.listdir(settings.MOBISIG_DATA + folder)

        if len(signature_list) != 0:
            # print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MOBISIG_DATA +
                                 folder + "/" + file1, usecols=settings.MOBISIG_FIELDS)

                if "GEN" in file1:
                    lengths.append(len(df))

    counts, bins = np.histogram(lengths, 100)

    plt.hist(bins[:-1], 100, weights=counts)
    plt.xlabel("Aláírások hossza")
    plt.ylabel("Aláírások száma")
    plt.savefig('images/mobisig_length.png')
    plt.clf()

    avg = np.average(lengths)
    print("MOBISIG average length: " + str(avg))

    std = 0
    for l in lengths:
        std += (l-avg)**2

    std /= len(lengths)

    std = std**0.5

    print("MOBISIG standard deviation: " + str(std))


def plot_diff():
    plt.rcParams.update({'font.size': 22})
    folder_list = utils.listdirs(settings.MCYT_DATA)
    folder = folder_list[2]
    signature_list = os.listdir(settings.MCYT_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MCYT_DATA +
                     folder + "/" + file1, usecols=settings.MCYT_FIELDS)
    # interpolation
    df = processing.interpolate_mcyt_file(df)

    r = np.arange(0, 512)

    df1 = df.values
    fig, axs = plt.subplots(2, 2)

    axs[0, 0].plot(r, df1[:, 0])
    axs[0, 0].set_title("Hossz normalizált x(t)")

    axs[1, 0].plot(r, df1[:, 1])
    axs[1, 0].set_title("Hossz normalizált y(t)")

    # calculating first order differences
    df = processing.first_order_substract(df)
    df = df.values

    axs[0, 1].plot(r, df[:, 0])
    axs[0, 1].set_title("x(t) sebessége")

    axs[1, 1].plot(r, df[:, 1])
    axs[1, 1].set_title("y(t) sebessége")
    fig.tight_layout()

    for ax in axs.flat:
        ax.set(xlabel='Idő', ylabel='Értékváltozás')

    fig.set_size_inches(18.5, 10.5)

    plt.savefig('images/diff.png', )
    plt.show()


def plot_signature():
    folder_list = utils.listdirs(settings.MCYT_DATA)
    folder = folder_list[2]
    signature_list = os.listdir(settings.MCYT_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MCYT_DATA +
                     folder + "/" + file1, usecols=settings.MCYT_FIELDS)
    df = df.values
    plt.plot(df[:, 0], df[:, 1])
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.savefig('images/signature_mcyt.png')
    plt.show()

    folder_list = utils.listdirs(settings.MOBISIG_DATA)
    folder = folder_list[0]
    signature_list = os.listdir(settings.MOBISIG_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MOBISIG_DATA +
                     folder + "/" + file1, usecols=settings.MOBISIG_FIELDS)
    df = df.values
    plt.plot(df[:, 0], df[:, 1]*(-1)+700)
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.savefig('images/signature_mobisig.png')
    plt.show()


def plot_interpolation():
    plt.rcParams.update({'font.size': 22})

    folder_list = utils.listdirs(settings.MCYT_DATA)
    folder = folder_list[2]
    signature_list = os.listdir(settings.MCYT_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MCYT_DATA +
                     folder + "/" + file1, usecols=settings.MCYT_FIELDS)

    df1 = df.values

    df = processing.interpolate_mcyt_file(df)
    df = df.values

    fig, axs = plt.subplots(2, 2)
    r1 = np.arange(0, len(df1[:, 0]))
    r2 = np.arange(0, 512)
    axs[0, 0].plot(r1, df1[:, 0])
    axs[0, 0].set_title("Hossz normalizálás előtti x(t)")
    axs[0, 1].plot(r2, df[:, 0])
    axs[0, 1].set_title("Hossz normalizálás utáni x'(t)")

    axs[1, 0].plot(r1, df1[:, 1])
    axs[1, 0].set_title("Hossz normalizálás előtti y(t)")
    axs[1, 1].plot(r2, df[:, 1])
    axs[1, 1].set_title("Hossz normalizálás utáni y'(t)")

    fig.tight_layout()

    for ax in axs.flat:
        ax.set(xlabel='Idő', ylabel='Értékváltozás')

    fig.set_size_inches(18.5, 10.5)

    plt.savefig('images/interpolation_mcyt.png', )
    plt.show()

    folder_list = utils.listdirs(settings.MOBISIG_DATA)
    folder = folder_list[0]
    signature_list = os.listdir(settings.MOBISIG_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MOBISIG_DATA +
                     folder + "/" + file1, usecols=settings.MOBISIG_FIELDS)

    df1 = df.values

    df = processing.interpolate_mobisig_file(df)
    df = df.values

    fig, axs = plt.subplots(2, 2)
    r1 = np.arange(0, len(df1[:, 0]))
    r2 = np.arange(0, 512)
    axs[0, 0].plot(r1, df1[:, 0])
    axs[0, 0].set_title("Hossz normalizálás előtti x(t)")
    axs[0, 1].plot(r2, df[:, 0])
    axs[0, 1].set_title("Hossz normalizálás utáni x'(t)")

    axs[1, 0].plot(r1, df1[:, 1])
    axs[1, 0].set_title("Hossz normalizálás előtti y(t)")
    axs[1, 1].plot(r2, df[:, 1])
    axs[1, 1].set_title("Hossz normalizálás utáni y'(t)")

    fig.tight_layout()

    for ax in axs.flat:
        ax.set(xlabel='Idő', ylabel='Értékváltozás')

    fig.set_size_inches(18.5, 10.5)

    plt.savefig('images/interpolation_mobisig.png', )
    plt.show()


def plot_normalized():
    plt.rcParams.update({'font.size': 22})
    folder_list = utils.listdirs(settings.MCYT_DATA)
    folder = folder_list[2]
    signature_list = os.listdir(settings.MCYT_DATA + folder)
    file1 = signature_list[0]
    df = pd.read_csv(settings.MCYT_DATA +
                     folder + "/" + file1, usecols=settings.MCYT_FIELDS)
    # interpolation
    df = processing.interpolate_mcyt_file(df)

    r = np.arange(0, 512)

    # calculating first order differences
    df = processing.first_order_substract(df)
    df1 = df.values
    df = df.values

    mu1 = np.mean(df[:, 0])
    sigma1 = np.std(df[:, 0])

    mu2 = np.mean(df[:, 1])
    sigma2 = np.std(df[:, 1])

    row1 = (df[:, 0] - mu1) / sigma1
    row2 = (df[:, 1] - mu2) / sigma2
    fig, axs = plt.subplots(2, 2)

    axs[0, 0].plot(r, df1[:, 0])
    axs[0, 0].set_title("x(t)")

    axs[1, 0].plot(r, df1[:, 1])
    axs[1, 0].set_title("y(t)")

    axs[0, 1].plot(r, row1)
    axs[0, 1].set_title("Normalizált x(t)")

    axs[1, 1].plot(r, row2)
    axs[1, 1].set_title("Normalizált y(t)")
    fig.tight_layout()

    for ax in axs.flat:
        ax.set(xlabel='Idő', ylabel='Értékváltozás')

    fig.set_size_inches(18.5, 10.5)

    plt.savefig('images/normalized.png', )
    plt.show()


def signature_lengths_in_seconds():
    # MCYT lengths
    folder_list = utils.listdirs(settings.MCYT_DATA)
    lengths = []
    for folder in folder_list:
        signature_list = os.listdir(settings.MCYT_DATA + folder)

        if len(signature_list) != 0:
            # print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MCYT_DATA +
                                 folder + "/" + file1, usecols=settings.MCYT_FIELDS)

                if(file1[4] == "v"):
                    lengths.append(len(df)/100)

    avg = np.average(lengths)
    print("MCYT average length: " + str(avg))

    std = 0
    for l in lengths:
        std += (l-avg)**2

    std /= len(lengths)

    std = std**0.5

    print("MCYT standard deviation: " + str(np.std(lengths)))

    # MOBISIG lengths
    folder_list = utils.listdirs(settings.MOBISIG_DATA)
    lengths = []
    for folder in folder_list:
        signature_list = os.listdir(settings.MOBISIG_DATA + folder)

        if len(signature_list) != 0:
            # print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MOBISIG_DATA +
                                 folder + "/" + file1, usecols=settings.MOBISIG_FIELDS)

                if "GEN" in file1:
                    lengths.append(len(df)/63)

    avg = np.average(lengths)
    print("MOBISIG average length: " + str(avg))

    print("MOBISIG standard deviation: " + str(np.std(lengths)))


# signature_lengths()
# plot_diff()
# plot_signature()
# plot_interpolation()
# plot_normalized()
# signature_lengths_in_seconds()
