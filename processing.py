import os
import csv
import numpy as np
import pandas as pd
from scipy import interpolate

import settings
import utils


def interpolate_mcyt_file(df):
    x = df['X']
    y = df[' Y']
    p = df[' P']
    N = len(df.index)

    # print("N: "+str(N))
    t = np.arange(0, N)
    dt = t[N-1]/settings.LENGTH
    tnew = np.arange(0, t[N-1], dt)
    # print(len(tnew))
    fx = interpolate.interp1d(t, x)
    fy = interpolate.interp1d(t, y)
    fp = interpolate.interp1d(t, p)

    xnew = fx(tnew)   # use interpolation function returned by `interp1d`
    ynew = fy(tnew)   # use interpolation function returned by `interp1d`
    pnew = fp(tnew)

    d = {'x': xnew, 'y': ynew, 'p': pnew}
    df = pd.DataFrame(data=d)
    # df.to_csv(outputfile, index=False)
    # plt.plot(xnew, 1000-ynew)
    # plt.show()
    return df


def interpolate_mobisig_file(df):
    x = df['x']
    y = df['y']
    t = df['timestamp']
    p = df['pressure']

    t = t - t[0]
    N = len(df.index)
    # print("N: "+str(N))
    t = np.arange(0, N)
    dt = t[N-1]/settings.LENGTH
    tnew = np.arange(0, t[N-1], dt)
    # print(len(tnew))
    fx = interpolate.interp1d(t, x)
    fy = interpolate.interp1d(t, y)
    fp = interpolate.interp1d(t, p)

    xnew = fx(tnew)   # use interpolation function returned by `interp1d`
    ynew = fy(tnew)   # use interpolation function returned by `interp1d`
    pnew = fp(tnew)

    d = {'x': xnew, 'y': ynew, 'p': pnew}
    df = pd.DataFrame(data=d)
    # df.to_csv(outputfile, index=False)
    # plt.plot(xnew, 1000-ynew)
    # plt.show()
    return df


def first_order_substract(df):
    result = df.diff()
    result.columns = ["x1", "y1", "p1"]
    result["x1"][0] = 0
    result["y1"][0] = 0
    result["p1"][0] = 0

    return result


def resample_mcyt():
    genuine_result = []
    forgery_result = []

    folder_list = utils.listdirs(settings.MCYT_DATA)

    for folder in folder_list:
        signature_list = os.listdir(settings.MCYT_DATA + folder)

        if len(signature_list) != 0:
            print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MCYT_DATA +
                                 folder + "/" + file1, usecols=settings.MCYT_FIELDS)

                # interpolation
                df = interpolate_mcyt_file(df)

                # calculating first order differences
                df = first_order_substract(df)
                df = df.values

                # resampling
                res = []
                res = np.append(res, df[:, 0])
                res = np.append(res, df[:, 1])
                res = np.append(res, df[:, 2])
                res = res.tolist()
                res.append(folder)

                if file1[4] == 'v':
                    genuine_result.append(res)
                else:
                    forgery_result.append(res)

    with open(settings.PROCESSED_DATA+"mcyt_genuine.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(genuine_result)

    with open(settings.PROCESSED_DATA+"mcyt_forgery.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(forgery_result)


def resample_mobisig():
    genuine_result = []
    forgery_result = []

    folder_list = utils.listdirs(settings.MOBISIG_DATA)

    for folder in folder_list:
        signature_list = os.listdir(settings.MOBISIG_DATA + folder)

        if len(signature_list) != 0:
            print("Current: " + folder)
            for file1 in signature_list:
                df = pd.read_csv(settings.MOBISIG_DATA +
                                 folder + "/" + file1, usecols=settings.MOBISIG_FIELDS)

                # interpolation
                df = interpolate_mobisig_file(df)

                # calculating first order differences
                df = first_order_substract(df)
                df = df.values

                # resampling
                res = []
                res = np.append(res, df[:, 0])
                res = np.append(res, df[:, 1])
                res = np.append(res, df[:, 2])
                res = res.tolist()
                res.append(folder)

                if "GEN" in file1:
                    genuine_result.append(res)
                else:
                    forgery_result.append(res)

    with open(settings.PROCESSED_DATA+"mobisig_genuine.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(genuine_result)

    with open(settings.PROCESSED_DATA+"mobisig_forgery.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(forgery_result)


def resample_all():
    resample_mcyt()
    resample_mobisig()
