import tensorflow.keras as keras
import tensorflow as tf
import numpy as np
import time
import os

import settings

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


def build_model(input_shape, nb_classes, num_filters=256, kernel_size=8):
    input_layer = keras.layers.Input(input_shape)

    conv1 = keras.layers.Conv1D(
        filters=num_filters, kernel_size=kernel_size, padding='same')(input_layer)
    conv1 = keras.layers.BatchNormalization()(conv1)
    conv1 = keras.layers.Activation(activation='relu')(conv1)

    conv2 = keras.layers.Conv1D(
        filters=2*num_filters, kernel_size=kernel_size-3, padding='same')(conv1)
    conv2 = keras.layers.BatchNormalization()(conv2)
    conv2 = keras.layers.Activation('relu')(conv2)

    conv3 = keras.layers.Conv1D(
        num_filters, kernel_size=kernel_size-5, padding='same')(conv2)
    conv3 = keras.layers.BatchNormalization()(conv3)
    conv3 = keras.layers.Activation('relu')(conv3)

    gap_layer = keras.layers.GlobalAveragePooling1D()(conv3)

    output_layer = keras.layers.Dense(
        nb_classes, activation='softmax')(gap_layer)

    model = keras.models.Model(inputs=input_layer, outputs=output_layer)

    model.compile(loss='categorical_crossentropy', optimizer=keras.optimizers.Adam(),
                  metrics=['accuracy'])

    model.summary()

    dot_img_file = './model.png'
    keras.utils.plot_model(model, to_file=dot_img_file,
                           show_shapes=True, show_layer_names=True)

    return model


def fit_model(model, x_train, y_train, x_val, y_val, y_true, verbose=True):
    # x_val and y_val are only used to monitor the test loss and NOT for training
    batch_size = 16
    nb_epochs = 100

    mini_batch_size = int(min(x_train.shape[0]/10, batch_size))

    reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.5, patience=50,
                                                  min_lr=0.0001)

    file_path = settings.MODEL_OUTPUT_DIRECTORY+'best_model.hdf5'

    model_checkpoint = keras.callbacks.ModelCheckpoint(filepath=file_path, monitor='loss',
                                                       save_best_only=True)

    start_time = time.time()

    hist = model.fit(x_train, y_train, batch_size=mini_batch_size, epochs=nb_epochs,
                     verbose=verbose, validation_data=(x_val, y_val), callbacks=[reduce_lr, model_checkpoint])

    duration = time.time() - start_time

    model.save(settings.MODEL_OUTPUT_DIRECTORY+'last_model.hdf5')

    model = keras.models.load_model(file_path)

    y_pred = model.predict(x_val)

    # convert the predicted from binary to integer
    y_pred = np.argmax(y_pred, axis=1)

    # save_logs(self.output_directory, hist, y_pred, y_true, duration)

    keras.backend.clear_session()


def predict(x_test):
    model_path = settings.MODEL_OUTPUT_DIRECTORY + 'best_model_mobisig.hdf5'
    model = keras.models.load_model(model_path)
    y_pred = model.predict(x_test)

    return y_pred


def get_extract_model(path):
    model_path = settings.MODEL_OUTPUT_DIRECTORY + path
    model = keras.models.load_model(model_path)

    feat_model = tf.keras.Sequential()
    for layer in model.layers[:-1]:
        feat_model.add(layer)
    return feat_model


def export_feature_extractor_to_tflite():
    # load model
    model = tf.keras.models.load_model(settings.MODEL_PATH)
    model.summary()

    # cut the last layer
    feat_model = tf.keras.Sequential()
    for layer in model.layers[:-1]:
        print(layer)
        feat_model.add(layer)
    feat_model.summary()

    # convert to tflite
    converter = tf.lite.TFLiteConverter.from_keras_model(feat_model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.target_spec.supported_types = [tf.double]
    tflite_model = converter.convert()

    # Save the model.
    with open(settings.MODEL_OUTPUT_PATH, 'wb') as f:
        f.write(tflite_model)
