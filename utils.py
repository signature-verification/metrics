import os
import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder

import settings
import model


def listdirs(folder):
    return [d for d in os.listdir(folder) if os.path.isdir(os.path.join(folder, d))]


def split_data(df):
    df = normalize_rows_signature(df, "ZSCORE")
    array = df.values

    nsamples, nfeatures = array.shape

    # nfeatures = nfeatures - 1
    nfeatures = 1024

    X = array[:, 0:nfeatures]
    y = array[:, -1]

    X = np.asarray(X).astype(np.float32)
    X = X.reshape(-1, settings.LENGTH, settings.DIMENSIONS)

    enc = OneHotEncoder()
    enc.fit(y.reshape(-1, 1))
    y = enc.transform(y.reshape(-1, 1)).toarray()

    # train, test = train_test_split(X, y, test_size=0.2)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=settings.RANDOM_STATE)

    X_train, X_val, y_train, y_val = train_test_split(
        X_train, y_train, test_size=0.25, random_state=settings.RANDOM_STATE)

    return X_train, y_train, X_test, y_test, X_val, y_val


def normalize_rows_signature(df, norm_type):
    array = df.values
    nsamples, nfeatures = array.shape
    nfeatures = nfeatures - 1
    X = array[:, 0:nfeatures]
    y = array[:, -1]

    rows, cols = X.shape
    if(norm_type == "MINMAX"):
        for i in range(0, rows):
            row = X[i, :]
            maxr = max(row)
            minr = min(row)
            if(maxr != minr):
                X[i, :] = (X[i, :] - minr) / (maxr - minr)
            else:
                X[i, :] = 1
    if(norm_type == "ZSCORE"):
        for i in range(0, rows):
            row = X[i, :]
            rows = np.array_split(row, 3)
            row1 = rows[0]
            row2 = rows[1]
            row3 = rows[2]
            mu1 = np.mean(row1)
            sigma1 = np.std(row1)

            mu1 = np.mean(row1)
            sigma1 = np.std(row1)

            mu2 = np.mean(row2)
            sigma2 = np.std(row2)

            mu3 = np.mean(row3)
            sigma3 = np.std(row3)

            row1 = (row1 - mu1) / sigma1
            row2 = (row2 - mu2) / sigma2
            row3 = (row3 - mu3) / sigma3

            X[i, :] = np.concatenate((row1, row2, row3), axis=0)

    df = pd.DataFrame(X)
    df['user'] = y
    return df


def get_accuracy(y_true, y_pred):
    y_pred = [y_pred[i][y_true[i]] for i in range(len(y_true))]

    return np.sum(y_pred) / len(y_true)


def test_filters(df):
    x_train, y_train, x_test, y_test, x_val, y_val = split_data(df)

    y_true = np.argmax(y_test, axis=1)

    nb_classes = y_test.shape[1]

    shape = x_train.shape[1:]

    result = []

    for filter_number in [32, 64, 128, 256]:
        fcn_model = model.build_model(shape, nb_classes, filter_number)

        fcn_model = model.fit_model(
            fcn_model, x_train, y_train, x_val, y_val, y_true)

        y_pred = model.predict(x_test)

        result.append([filter_number, get_accuracy(y_true, y_pred)])

    df_result = pd.DataFrame(result, columns=['filter_number', 'score'])
    df_result.to_csv("./filter_test.csv")


def test_kernel_size(df):
    x_train, y_train, x_test, y_test, x_val, y_val = split_data(df)

    y_true = np.argmax(y_test, axis=1)

    nb_classes = y_test.shape[1]

    shape = x_train.shape[1:]

    result = []

    for kernel_size in [8, 9, 10, 11, 12]:
        fcn_model = model.build_model(shape, nb_classes, 256, kernel_size)

        fcn_model = model.fit_model(
            fcn_model, x_train, y_train, x_val, y_val, y_true)

        y_pred = model.predict(x_test)

        result.append([get_accuracy(y_true, y_pred)])

    df_result = pd.DataFrame(result, columns=['kernel_size', 'score'])
    df_result.to_csv("./kernel_size_test.csv")
