import os
import model
import processing
import pandas as pd
import numpy as np

import settings
import utils


os.environ['CUDA_VISIBLE_DEVICES'] = "0"

# processing.resample_all()


# df = pd.read_csv(settings.MCYT_GENUINE, header=None)

# print(df.shape)

# x_train, y_train, x_test, y_test, x_val, y_val = utils.split_data(df)

# y_true = np.argmax(y_test, axis=1)

# nb_classes = y_test.shape[1]

# shape = x_train.shape[1:]

# fcn_model = model.build_model(shape, nb_classes, 256)

# fcn_model = model.fit_model(fcn_model, x_train, y_train, x_val, y_val, y_true)

# y_pred = model.predict(x_test)

# print(utils.get_accuracy(y_true, y_pred))

model.export_feature_extractor_to_tflite()

# TESTING
# utils.test_filters(df)
# utils.test_kernel_size(df)
